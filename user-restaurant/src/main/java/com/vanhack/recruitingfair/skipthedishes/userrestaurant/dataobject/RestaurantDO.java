package com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.domainvalue.TypeOfCulinary;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class RestaurantDO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private TypeOfCulinary typeOfCulinary;
    private String address;

    public RestaurantDO(){}

    public RestaurantDO(String name, TypeOfCulinary typeOfCulinary, String address) {
        this.name = name;
        this.typeOfCulinary = typeOfCulinary;
        this.address = address;
    }
}
