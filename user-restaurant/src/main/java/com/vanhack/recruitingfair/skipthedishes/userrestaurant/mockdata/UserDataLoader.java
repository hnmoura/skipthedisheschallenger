package com.vanhack.recruitingfair.skipthedishes.userrestaurant.mockdata;

import com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject.UserDO;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.domainvalue.TypeOfCulinary;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;

@Component
public class UserDataLoader implements ApplicationRunner {

    private UserService userService;

    @Autowired
    public UserDataLoader(UserService userService){
        this.userService = userService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        this.userService.createUser(new UserDO("hnmoura","Hivison N Moura",
                "Rua: Jasmim, 310, Chácara Primave, 132 B",
                new HashSet<>(Arrays.asList(TypeOfCulinary.ASIAN,TypeOfCulinary.BRAZILIAN))));
        this.userService.createUser(new UserDO("gabrielOest","Gabriel Oest",
                "Rua: Jasmim, 310, Chácara Primave, 132 B",
               new HashSet<>(Arrays.asList(TypeOfCulinary.EUROPEAN,TypeOfCulinary.BRAZILIAN
                    ,TypeOfCulinary.INDIAN))));

    }
}
