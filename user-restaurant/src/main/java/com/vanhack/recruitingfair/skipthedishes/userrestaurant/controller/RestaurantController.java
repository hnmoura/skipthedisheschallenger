package com.vanhack.recruitingfair.skipthedishes.userrestaurant.controller;

import com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject.RestaurantDO;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/restaurants")
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    @GetMapping
    public List<RestaurantDO> findAllRestaurants(){
        return restaurantService.findAllRestaurants();
    }

    @GetMapping("/{restaurantName}")
    public RestaurantDO findRestaurantByName(@PathVariable String restaurantName){
        return restaurantService.findByName(restaurantName).get();
    }

    @PostMapping
    public RestaurantDO createRestaurant(@RequestBody RestaurantDO restaurantDO){
        return restaurantService.createRestaurant(restaurantDO);
    }
}
