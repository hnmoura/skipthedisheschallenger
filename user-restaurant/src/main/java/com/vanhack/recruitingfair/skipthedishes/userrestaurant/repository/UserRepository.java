package com.vanhack.recruitingfair.skipthedishes.userrestaurant.repository;

import com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject.UserDO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserDO, Long>{
}
