package com.vanhack.recruitingfair.skipthedishes.userrestaurant.service;

import com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject.RestaurantDO;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.exception.RestaurantNotFoundException;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    public List<RestaurantDO> findAllRestaurants(){
        return restaurantRepository.findAll();
    }

    public Optional<RestaurantDO> findByName(String restaurant){
        return Optional.ofNullable(restaurantRepository.findByName(restaurant))
                .orElseThrow(() ->  new RestaurantNotFoundException("Restaurant not found. Restaurant: " + restaurant));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public RestaurantDO createRestaurant(RestaurantDO restaurantDO){
        final RestaurantDO newRestaurantDO = new RestaurantDO();
        newRestaurantDO.setName(restaurantDO.getName());
        newRestaurantDO.setAddress(restaurantDO.getAddress());
        newRestaurantDO.setTypeOfCulinary(restaurantDO.getTypeOfCulinary());

        return restaurantRepository.save(newRestaurantDO);
    }
}
