package com.vanhack.recruitingfair.skipthedishes.userrestaurant.service;

import com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject.RestaurantDO;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject.UserDO;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public UserDO createUser(UserDO userDO){
        final UserDO newUserDO = new UserDO();
        newUserDO.setName(userDO.getName());
        newUserDO.setAddress(userDO.getAddress());
        newUserDO.setUserName(userDO.getUserName());
        newUserDO.setPreferencesCulinary(userDO.getPreferencesCulinary());
        return userRepository.save(newUserDO);
    }
}
