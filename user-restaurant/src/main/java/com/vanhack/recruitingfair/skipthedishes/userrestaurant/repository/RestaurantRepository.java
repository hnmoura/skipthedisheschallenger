package com.vanhack.recruitingfair.skipthedishes.userrestaurant.repository;

import com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject.RestaurantDO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RestaurantRepository extends JpaRepository<RestaurantDO, Long> {

        Optional<RestaurantDO> findByName(String name);
}
