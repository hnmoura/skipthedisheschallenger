package com.vanhack.recruitingfair.skipthedishes.userrestaurant.mockdata;


import com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject.RestaurantDO;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject.UserDO;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.domainvalue.TypeOfCulinary;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.service.RestaurantService;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;

@Component
public class RestaurantDataLoader implements ApplicationRunner {

    private RestaurantService restaurantService;

    @Autowired
    public RestaurantDataLoader(RestaurantService restaurantService){
        this.restaurantService = restaurantService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        this.restaurantService.createRestaurant(new RestaurantDO("Subway",TypeOfCulinary.ASIAN,
                "Rua: Jasmim, 310, Chácara Primave, 132 B"));
        this.restaurantService.createRestaurant(new RestaurantDO("Red Lobster",TypeOfCulinary.EUROPEAN,
                "Rua Tesste"));

    }
}
