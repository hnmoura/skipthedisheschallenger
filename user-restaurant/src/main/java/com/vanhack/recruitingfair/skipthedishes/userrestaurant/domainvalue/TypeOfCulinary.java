package com.vanhack.recruitingfair.skipthedishes.userrestaurant.domainvalue;

public enum TypeOfCulinary {
    ASIAN,INDIAN,BRAZILIAN,EUROPEAN
}
