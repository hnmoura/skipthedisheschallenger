package com.vanhack.recruitingfair.skipthedishes.userrestaurant.dataobject;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vanhack.recruitingfair.skipthedishes.userrestaurant.domainvalue.TypeOfCulinary;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UserDO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String userName;
    private String name;
    private String address;

    @ElementCollection(targetClass = TypeOfCulinary.class)
    private Set<TypeOfCulinary> preferencesCulinary;

    public  UserDO(){}

    public UserDO(String userName, String name, String address, Set<TypeOfCulinary> preferencesCulinary) {
        this.userName = userName;
        this.name = name;
        this.address = address;
        this.preferencesCulinary = preferencesCulinary;
    }
}
