package com.vanhack.recruitingfair.skipthedishes.rest.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vanhack.recruitingfair.skipthedishes.rest.domain.Place;
import com.vanhack.recruitingfair.skipthedishes.rest.service.RestaurantService;

@RestController
public class RestaurantController {
	
	@Autowired
	private RestaurantService restaurantService;

	@RequestMapping("/surpriseDishes")
	public ResponseEntity<Place> requestDishe(@RequestParam(value = "geocode") String geocode,
			@RequestParam(value = "radius") String radius) {
		
		Set<Place> nearByRestaurants = restaurantService.getNearByRestaurants(geocode, radius);
		
		Place nearestPlace = restaurantService.getNearestRestaurant(geocode, nearByRestaurants);

		Place restaurantByPlaceId = restaurantService.getRestaurantByPlaceId(nearestPlace.getPlaceId());
		
		ResponseEntity<Place> response = new ResponseEntity<>(restaurantByPlaceId, HttpStatus.OK);

		return response;
	}
}
