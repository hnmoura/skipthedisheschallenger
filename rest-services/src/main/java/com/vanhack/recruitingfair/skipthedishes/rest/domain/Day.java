package com.vanhack.recruitingfair.skipthedishes.rest.domain;

/**
 * Represents a day in the week.
 */
public enum Day {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY
}
