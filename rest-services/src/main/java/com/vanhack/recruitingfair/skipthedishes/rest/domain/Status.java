package com.vanhack.recruitingfair.skipthedishes.rest.domain;

/**
 * Represents the current status of the place in time.
 */
public enum Status {
    OPENED,
    CLOSED,
    NONE
}
