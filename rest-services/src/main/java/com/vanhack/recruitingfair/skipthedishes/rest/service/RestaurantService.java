package com.vanhack.recruitingfair.skipthedishes.rest.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.vanhack.recruitingfair.skipthedishes.rest.domain.Place;
import com.vanhack.recruitingfair.skipthedishes.rest.domain.Status;

@Service
public class RestaurantService {
	
	@Autowired
	private JsonRestaurantExtractionUtil jsonRestaurantExtractionUtil;

	public Set<Place> getNearByRestaurants(String geocode, String radius) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = String.format(
				"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%s&radius=%s&type=restaurant&keyword=japanese&key=AIzaSyD1fTEGlz9uJGmlnnHfFto_YOnHKBRkKGg",
				geocode, radius);
		String response = restTemplate.getForObject(fooResourceUrl, String.class);

		return jsonRestaurantExtractionUtil.extractResults(response);
	}

	public Place getRestaurantByPlaceId(@RequestParam(value = "placeid") String placeId) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = String.format(
				"https://maps.googleapis.com/maps/api/place/details/json?placeid=%s&key=AIzaSyD1fTEGlz9uJGmlnnHfFto_YOnHKBRkKGg",
				placeId);

		String response = restTemplate.getForObject(fooResourceUrl, String.class);

		return jsonRestaurantExtractionUtil.createPlace(response);
	}
	
	public Place getNearestRestaurant(String geocode, Collection<Place> nearByRestaurants) {
		String[] split = geocode.split(",");
		Double lat = Double.valueOf(split[0]);
		Double lng = Double.valueOf(split[1]);
		
		List<Place> openedRestaurants = nearByRestaurants.stream().filter(it -> it.getStatus().equals(Status.OPENED))
				.collect(Collectors.toList());
		
		return jsonRestaurantExtractionUtil.findNearest(lat, lng, openedRestaurants);
	}

}
