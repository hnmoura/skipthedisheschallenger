package com.vanhack.recruitingfair.skipthedishes.rest.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.vanhack.recruitingfair.skipthedishes.rest.domain.Day;
import com.vanhack.recruitingfair.skipthedishes.rest.domain.Hours;
import com.vanhack.recruitingfair.skipthedishes.rest.domain.Place;
import com.vanhack.recruitingfair.skipthedishes.rest.domain.Status;
import com.vanhack.recruitingfair.skipthedishes.rest.util.Util;

@Component
public class JsonRestaurantExtractionUtil {

	
	private static final String RESULT = "result";
	private static final String RESULTS = "results";
	private static final String PLACE_ID = "place_id";
	private static final String NAME = "name";
	private static final String OPENNING_TIME = "0000";
	private static final String OPEN_NOW = "open_now";
	private static final String CLOSE = "close";
	private static final String TIME = "time";
	private static final String DAY = "day";
	private static final String OPEN = "open";
	private static final String PERIODS = "periods";
	private static final String WEBSITE = "website";
	private static final String VICINITY = "vicinity";
	private static final String URL = "url";
	private static final String RATING = "rating";
	private static final String INTERNATIONAL_PHONE_NUMBER = "international_phone_number";
	private static final String FORMATTED_PHONE_NUMBER = "formatted_phone_number";
	private static final String FORMATTED_ADDRESS = "formatted_address";
	private static final String OPENING_HOURS = "opening_hours";
	private static final String LONGITUDE = "lng";
	private static final String LATITUDE = "lat";
	private static final String LOCATION = "location";
	private static final String GEOMETRY = "geometry";

	public Set<Place> extractResults(String response) {
		Set<Place> places = new HashSet<>();
		try {
			JSONObject json = new JSONObject(response);
			JSONArray arrayResults = json.getJSONArray(RESULTS);

			for (int i = 0; i < arrayResults.length(); i++) {
				JSONObject result = arrayResults.getJSONObject(i);
				Place place = createBasicPlace(result);

				places.add(place);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return places;
	}

	private Place createBasicPlace(JSONObject result) throws JSONException {
		String name = result.getString(NAME);
		String placeId = result.getString(PLACE_ID);
		JSONObject location = result.getJSONObject(GEOMETRY).getJSONObject(LOCATION);
		double lat = location.getDouble(LATITUDE);
		double lng = location.getDouble(LONGITUDE);
		JSONObject hours = result.optJSONObject(OPENING_HOURS);
		Status status = createStatus(hours);

		Place place = new Place(name, placeId, lat, lng, status);
		return place;
	}

	public Place createPlace(String response) {
		Place place = null;
		try {
			JSONObject json = new JSONObject(response);
			JSONObject result = json.getJSONObject(RESULT);

			String name = result.getString(NAME);
			String id = result.getString(PLACE_ID);
			String address = result.optString(FORMATTED_ADDRESS, null);
			String phone = result.optString(FORMATTED_PHONE_NUMBER, null);
			String internationalPhone = result.optString(INTERNATIONAL_PHONE_NUMBER, null);
			double rating = result.optDouble(RATING, -1);
			String url = result.optString(URL, null);
			String vicinity = result.optString(VICINITY, null);
			String website = result.optString(WEBSITE, null);

			JSONObject location = result.getJSONObject(GEOMETRY).getJSONObject(LOCATION);
			double lat = location.getDouble(LATITUDE), lng = location.getDouble(LONGITUDE);

			JSONObject hours = result.optJSONObject(OPENING_HOURS);
			Status status = createStatus(hours);

			Hours schedule = createSchedule(hours);

			place = new Place(name, id, address, phone, internationalPhone, rating, url, vicinity, website, lat, lng,
					status, schedule);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return place;
	}

	private Hours createSchedule(JSONObject hours) throws JSONException {
		Hours schedule = new Hours();
		if (hours != null) {
			// periods of operation
			JSONArray jsonPeriods = hours.optJSONArray(PERIODS);
			if (jsonPeriods != null) {
				for (int i = 0; i < jsonPeriods.length(); i++) {
					JSONObject jsonPeriod = jsonPeriods.getJSONObject(i);

					// opening information (from)
					JSONObject opens = jsonPeriod.getJSONObject(OPEN);
					Day openingDay = Day.values()[opens.getInt(DAY)];
					String openingTime = opens.getString(TIME);

					// if this place is always open, break.
					boolean alwaysOpened = openingDay == Day.SUNDAY && openingTime.equals(OPENNING_TIME)
							&& !jsonPeriod.has(CLOSE);
					if (alwaysOpened) {
						schedule.setAlwaysOpened(true);
						break;
					}

					// closing information (to)
					JSONObject closes = jsonPeriod.getJSONObject(CLOSE);
					Day closingDay = Day.values()[closes.getInt(DAY)]; // to
					String closingTime = closes.getString(TIME);

					// add the period to the hours
					schedule.addPeriod(new Hours.Period().setOpeningDay(openingDay).setOpeningTime(openingTime)
							.setClosingDay(closingDay).setClosingTime(closingTime));
				}
			}
		}
		return schedule;
	}

	private Status createStatus(JSONObject hours) {
		Status status = Status.NONE;
		try {
			if (hours != null) {
				boolean statusDefined = hours.has(OPEN_NOW);
				status = statusDefined && hours.getBoolean(OPEN_NOW) ? Status.OPENED : Status.CLOSED;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return status;
	}

	public Place findNearest(double lat, double lng, List<Place> places) {
		// customer latitude and longitude
		double lat1 = lat;
		double lon1 = lng;
		// hold the nearest distance found till now
		double nearestDist = -1;
		// hold the reference to the nearest restaurant found till now
		Place nearestRestaurant = null;

		for (Place restaurant : places) {
			// latitude and longitude of the restaurant to compare
			double lat2 = restaurant.getLat();
			double lon2 = restaurant.getLng();
			// distance to the restaurant in comparison
			double dist = Util.haversine(lat1, lon1, lat2, lon2);
			// if the restaurant in comparison is nearer than the previous restaurant or if
			// it is the first shop
			if (dist < nearestDist || nearestDist == -1) {
				nearestRestaurant = restaurant;
				nearestDist = dist;
			}
		}

		return nearestRestaurant;
	}
}
