package com.vanhack.recruitingfair.skipthedishes.rest.domain;

public class Place {

	String name;
	String id;
	String address;
	String phone;
	String internationalPhone;
	double rating;
	String url;
	String vicinity;
	String website;
	private String placeId;

	double lat;
	double lng;
	Status status = Status.NONE;
	Hours schedule = new Hours();
	
	public Place(String name, String placeId, double lat, double lng, Status status) {
		this.name = name;
		this.placeId = placeId;
		this.lat = lat;
		this.lng = lng;
		this.status = status;
	}
	
	public Place(String name, String id, String address, String phone, String internationalPhone, double rating,
			String url, String vicinity, String website, double lat, double lng, Status status, Hours schedule) {
		super();
		this.name = name;
		this.id = id;
		this.address = address;
		this.phone = phone;
		this.internationalPhone = internationalPhone;
		this.rating = rating;
		this.url = url;
		this.vicinity = vicinity;
		this.website = website;
		this.lat = lat;
		this.lng = lng;
		this.status = status;
		this.schedule = schedule;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getInternationalPhone() {
		return internationalPhone;
	}
	public void setInternationalPhone(String internationalPhone) {
		this.internationalPhone = internationalPhone;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getVicinity() {
		return vicinity;
	}
	public void setVicinity(String vicinity) {
		this.vicinity = vicinity;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Hours getSchedule() {
		return schedule;
	}
	public void setSchedule(Hours schedule) {
		this.schedule = schedule;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}
}
