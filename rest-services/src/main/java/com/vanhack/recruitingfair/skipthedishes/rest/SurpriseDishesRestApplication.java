package com.vanhack.recruitingfair.skipthedishes.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurpriseDishesRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SurpriseDishesRestApplication.class, args);
    }
}
